/****************************** Module Header ******************************\
* Module Name:  SampleService.h
* Project:      CppWindowsService
* Copyright (c) Microsoft Corporation.
* 
* Provides a sample service class that derives from the service base class - 
* CServiceBase. The sample service logs the service start and stop 
* information to the Application event log, and shows how to run the main 
* function of the service in a thread pool worker thread.
* 
* This source is subject to the Microsoft Public License.
* See http://www.microsoft.com/en-us/openness/resources/licenses.aspx#MPL.
* All other rights reserved.
* 
* THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
* EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
* WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

#pragma once

#include "ServiceBase.h"
#include <string>
#include <list>

class CNoMoreGameService : public CServiceBase
{
public:

    CNoMoreGameService(PWSTR pszServiceName, 
        BOOL fCanStop = TRUE, 
        BOOL fCanShutdown = TRUE, 
        BOOL fCanPauseContinue = FALSE);
    virtual ~CNoMoreGameService(void);

protected:

    virtual void OnStart(DWORD dwArgc, LPWSTR *pszArgv);
    virtual void OnStop();

    void ServiceWorkerThread(void);
	
private:

    BOOL m_fStopping;
    HANDLE m_hStoppedEvent;

	//Counters
	bool isGame = false;
	time_t currentTime;
	time_t lastTime;
	time_t gameTime;


	//Registry
	HKEY hKey;
	void initRegistry();
	void deinitRegistry() const;
	void setRegistry(const std::wstring& name, time_t value) const;
	time_t getRegistry(const std::wstring& name) const;

	void readGameTime();
	void readLastTime();
	void readGamesList();
	void storeGameTime() const;
	void storeLastTime() const;

	//check
	void checkNextDay();
	void checkGameTime();
    bool checkContinuesTime( const std::wstring& process );    

	void storeCounters() const;

	std::list <std::wstring> deadProcess;
};

const wchar_t REGISTRY_KEY[] = L"SYSTEM\\CurrentControlSet\\services\\NMGService";
const wchar_t PARAMETERS[] = L"Parameters";
const wchar_t GAME_TIME[] = L"gameTime";
const wchar_t LAST_TIME[] = L"lastTime";
const wchar_t GAME_NAMES[] = L"gameNames";

static const time_t MAX_GAME_TIME = 10800; //3 ����
static const time_t REMIAND_BEFORE = 600;//10 �����
static const int LATE_HOURS = 22;
static const int EARLY_HOURS = 9; 
