/****************************** Module Header ******************************\
* Module Name:  SampleService.cpp
* Project:      CppWindowsService
* Copyright (c) Microsoft Corporation.
* 
* Provides a sample service class that derives from the service base class - 
* CServiceBase. The sample service logs the service start and stop 
* information to the Application event log, and shows how to run the main 
* function of the service in a thread pool worker thread.
* 
* This source is subject to the Microsoft Public License.
* See http://www.microsoft.com/en-us/openness/resources/licenses.aspx#MPL.
* All other rights reserved.
* 
* THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
* EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
* WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/
#pragma region Includes
#include "NMGService.h"
#include "ThreadPool.h"
#pragma endregion


#include "resource.h"


#include <windows.h>
#include <process.h>
#include <Tlhelp32.h>
#include <winbase.h>
#include <string.h>
#include <time.h>

#include <Commctrl.h>
#include <Strsafe.h>

CNoMoreGameService::CNoMoreGameService(PWSTR pszServiceName, 
                               BOOL fCanStop, 
                               BOOL fCanShutdown, 
                               BOOL fCanPauseContinue)
: CServiceBase(pszServiceName, fCanStop, fCanShutdown, fCanPauseContinue)
{
    m_fStopping = FALSE;

    // Create a manual-reset event that is not signaled at first to indicate 
    // the stopped signal of the service.
    m_hStoppedEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    if (m_hStoppedEvent == NULL)
    {
        throw GetLastError();
    }
}


CNoMoreGameService::~CNoMoreGameService(void)
{
    if (m_hStoppedEvent)
    {
        CloseHandle(m_hStoppedEvent);
        m_hStoppedEvent = NULL;
    }
}


//
//   FUNCTION: CNoMoreGameService::OnStart(DWORD, LPWSTR *)
//
//   PURPOSE: The function is executed when a Start command is sent to the 
//   service by the SCM or when the operating system starts (for a service 
//   that starts automatically). It specifies actions to take when the 
//   service starts. In this code sample, OnStart logs a service-start 
//   message to the Application log, and queues the main service function for 
//   execution in a thread pool worker thread.
//
//   PARAMETERS:
//   * dwArgc   - number of command line arguments
//   * lpszArgv - array of command line arguments
//
//   NOTE: A service application is designed to be long running. Therefore, 
//   it usually polls or monitors something in the system. The monitoring is 
//   set up in the OnStart method. However, OnStart does not actually do the 
//   monitoring. The OnStart method must return to the operating system after 
//   the service's operation has begun. It must not loop forever or block. To 
//   set up a simple monitoring mechanism, one general solution is to create 
//   a timer in OnStart. The timer would then raise events in your code 
//   periodically, at which time your service could do its monitoring. The 
//   other solution is to spawn a new thread to perform the main service 
//   functions, which is demonstrated in this code sample.
//
void CNoMoreGameService::OnStart(DWORD dwArgc, LPWSTR *lpszArgv)
{
    // Log a service start message to the Application log.
    WriteEventLogEntry(L"NMGService in OnStart", 
        EVENTLOG_INFORMATION_TYPE);

	initRegistry();
	
	// Queue the main service function for execution in a worker thread.
    CThreadPool::QueueUserWorkItem(&CNoMoreGameService::ServiceWorkerThread, this);
}

//
//   FUNCTION: CNoMoreGameService::ServiceWorkerThread(void)
//
//   PURPOSE: The method performs the main function of the service. It runs 
//   on a thread pool worker thread.
//

void CNoMoreGameService::ServiceWorkerThread(void)
{
	
    // Periodically check if the service is stopping.
    while (!m_fStopping)
    {
		checkGameTime();
        ::Sleep(5000);  // 5 ������
    }

    // Signal the stopped event.
    SetEvent(m_hStoppedEvent);
}

//
//   FUNCTION: CNoMoreGameService::OnStop(void)
//
//   PURPOSE: The function is executed when a Stop command is sent to the 
//   service by SCM. It specifies actions to take when a service stops 
//   running. In this code sample, OnStop logs a service-stop message to the 
//   Application log, and waits for the finish of the main service function.
//
//   COMMENTS:
//   Be sure to periodically call ReportServiceStatus() with 
//   SERVICE_STOP_PENDING if the procedure is going to take long time. 
//
void CNoMoreGameService::OnStop()
{
	// Log a service stop message to the Application log.
	WriteEventLogEntry(L"NMGService in OnStop",
		EVENTLOG_INFORMATION_TYPE);

	// Indicate that the service is stopping and wait for the finish of the 
	// main service function (ServiceWorkerThread).
	m_fStopping = TRUE;
	if (WaitForSingleObject(m_hStoppedEvent, INFINITE) != WAIT_OBJECT_0)
	{
		throw GetLastError();
	}

	deinitRegistry();
}


void killProcessByName(const std::wstring& filename)
{
	HANDLE hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPALL, NULL);
	PROCESSENTRY32 pEntry;
	pEntry.dwSize = sizeof(pEntry);
	BOOL hRes = Process32First(hSnapShot, &pEntry);
	while (hRes)
	{
		if (wcscmp(pEntry.szExeFile, filename.c_str()) == 0)
		{
			HANDLE hProcess = OpenProcess(PROCESS_TERMINATE, 0,
				(DWORD)pEntry.th32ProcessID);
			if (hProcess != NULL)
			{
				TerminateProcess(hProcess, 9);
				CloseHandle(hProcess);
			}
		}
		hRes = Process32Next(hSnapShot, &pEntry);
	}
	CloseHandle(hSnapShot);
}

bool foundProcessByName(const std::wstring& filename)
{
	HANDLE hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPALL, NULL);
	PROCESSENTRY32 pEntry;
	pEntry.dwSize = sizeof(pEntry);
	BOOL hRes = Process32First(hSnapShot, &pEntry);

	bool result = false;
	while (hRes)
	{
		if (wcscmp(pEntry.szExeFile, filename.c_str()) == 0) {
			result = true;
			break;
		}
		hRes = Process32Next(hSnapShot, &pEntry);
	}
	CloseHandle(hSnapShot);

	return result;
}

void CNoMoreGameService::checkNextDay()
{
	tm timeinfo;
	tm timeinfo_last;
	localtime_s(&timeinfo, &currentTime);
	localtime_s(&timeinfo_last, &lastTime);

	if (timeinfo.tm_yday != timeinfo_last.tm_yday) {
		gameTime = 0;
		lastTime = 0;
	}
}

void CNoMoreGameService::checkGameTime() {
	
	currentTime = time(NULL);
	checkNextDay();

	tm timeinfo;
	localtime_s(&timeinfo, &currentTime);

	if (timeinfo.tm_hour > LATE_HOURS || timeinfo.tm_hour < EARLY_HOURS) {
		//��� �������� �� ������� ��� ���������� ��������
		isGame = false;

		for (auto process : deadProcess)
		{
			killProcessByName(process);
		}
	}
	else {
		for (auto process : deadProcess)
		{
			if (checkContinuesTime(process)) break; //���� ����� ���� ���� ����, ������ ������ ������ ��� - ������� ����� �� �����������.
		}
	}
	lastTime = currentTime;
	storeCounters(); //��������� ��������, ��� �� �� �������� �� ��� ������������
}


bool CNoMoreGameService::checkContinuesTime( const std::wstring& process ) {
	bool result = false;

	if (gameTime > MAX_GAME_TIME) {
		killProcessByName(process);
	}
	else {
		if (foundProcessByName(process)) { //���� ����
			if (isGame) { //������?
				gameTime += currentTime - lastTime;
			}
			else {
				isGame = true;
			}
			result = true; //���������� ������, ���� ���� ������� � ������ ���� ������ �� ����
		}
	}
	return result;
}

void CNoMoreGameService::storeCounters() const {
	storeGameTime();
	storeLastTime();
}

void CNoMoreGameService::initRegistry() {
	LONG lRes = RegCreateKeyEx(HKEY_LOCAL_MACHINE, REGISTRY_KEY, 0, nullptr, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, nullptr, &hKey, nullptr);

	readGamesList();
	readGameTime();
	readLastTime();

	currentTime = time(NULL);
	checkNextDay();
	lastTime = currentTime;
}

void CNoMoreGameService::deinitRegistry() const {
	RegCloseKey(hKey);
}


time_t CNoMoreGameService::getRegistry(const std::wstring& name)  const{

	time_t data;
	DWORD data_size = sizeof(data);

	if (RegGetValue(hKey, PARAMETERS, name.c_str(), RRF_RT_REG_QWORD, nullptr, &data, &data_size) == ERROR_SUCCESS) {
		return data;
	}
	return 0;
};

void CNoMoreGameService::setRegistry(const std::wstring& name, time_t value) const {
	RegSetKeyValue(hKey, PARAMETERS, name.c_str(), REG_QWORD, &value, sizeof(value));
}

inline void CNoMoreGameService::readGameTime() {

	gameTime = getRegistry(GAME_TIME);
};

inline void CNoMoreGameService::readLastTime() {
	lastTime = getRegistry(LAST_TIME);
};

inline void CNoMoreGameService::storeGameTime() const {
	setRegistry(GAME_TIME, gameTime);
};
inline void CNoMoreGameService::storeLastTime() const {
	setRegistry(LAST_TIME, lastTime);
};


void CNoMoreGameService::readGamesList(){
	static const int BUF_SIZE = 2048;
	DWORD buffer_size = BUF_SIZE*sizeof(wchar_t);
	wchar_t buffer[BUF_SIZE];

	if (RegGetValue(hKey, PARAMETERS, GAME_NAMES, RRF_RT_REG_MULTI_SZ, nullptr, buffer, &buffer_size) == ERROR_SUCCESS) {
		wchar_t *pszz = buffer;
		while (*pszz)
		{
			deadProcess.push_back(std::wstring(pszz));
			pszz = pszz + wcslen(pszz) + 1;
		}
	}
	else
	{
		deadProcess.push_back(L"swtor.exe");
		deadProcess.push_back(L"wildstar.exe");
		deadProcess.push_back(L"inqusition.exe");
	}
}