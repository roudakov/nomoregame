// nmgtray.cpp: ���������� ����� ����� ��� ����������.
//

#include "stdafx.h"
#include "resource.h"

#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#include <Strsafe.h>
#include <Commctrl.h>
#include <sapi.h>


#include <sstream>
#include <iomanip>


#include "nmgtray.h"
#include "..\nmgservice\NMGService.h"

static const wchar_t BIG_BRO[] = L"The Big Brother is watching you!";

NMGTray::NMGTray(HWND hWnd)
{
	this->hWnd = hWnd;
	initRegistry();
	initNotify();
	notify();
	SetTimer(hWnd,             // handle to main window 
		110,			                   // timer identifier 
		60000,                           // 60-second interval 
		(TIMERPROC)NULL);
}

NMGTray::~NMGTray()
{
	KillTimer(hWnd, 110);
	deinitRegistry();
	deinitNotify();
}

void NMGTray::initRegistry() {
	LONG lRes = RegCreateKeyEx(HKEY_LOCAL_MACHINE, REGISTRY_KEY, 0, nullptr, REG_OPTION_NON_VOLATILE, KEY_READ, nullptr, &hKey, nullptr);
}

void NMGTray::deinitRegistry() const {
	RegCloseKey(hKey);
}

void NMGTray::initNotify() {
	// {80328865-1D8F-419A-A129-E43381E436EA}
	static const GUID myGUID = { 0x80328865, 0x1d8f, 0x419a,{ 0xa1, 0x29, 0xe4, 0x33, 0x81, 0xe4, 0x36, 0xea } };

	nid.cbSize = sizeof(nid);
	nid.hWnd = hWnd;
	nid.uFlags = NIF_ICON | NIF_TIP | NIF_GUID | NIF_MESSAGE;
	nid.guidItem = myGUID;
	nid.uVersion = NOTIFYICON_VERSION_4;
	StringCchCopy(nid.szTip, ARRAYSIZE(nid.szTip), BIG_BRO);

	nid.uCallbackMessage = IDC_TRAY;

	// Load the icon for high DPI.
	LoadIconMetric(GetModuleHandle(nullptr), MAKEINTRESOURCE(IDI_NMGTRAY), LIM_SMALL, &(nid.hIcon));
	nid.hBalloonIcon = nid.hIcon;

	Shell_NotifyIcon(NIM_ADD, &nid);
}


void speek(const std::wstring& msg) {
	ISpVoice * pVoice = NULL;

	if (FAILED(::CoInitialize(NULL)))
		return;

	HRESULT hr = CoCreateInstance(CLSID_SpVoice, NULL, CLSCTX_ALL, IID_ISpVoice, (void **)&pVoice);
	if (SUCCEEDED(hr))
	{
		hr = pVoice->Speak(msg.c_str(), 0, NULL);
		pVoice->Release();
		pVoice = NULL;
	}

	::CoUninitialize();
}


time_t getTime(int h, int m)
{
	time_t rawtime;
	struct tm timeinfo;

	time(&rawtime);
	localtime_s(&timeinfo, &rawtime);
	timeinfo.tm_hour = h;
	timeinfo.tm_min = m;

	/* call mktime: timeinfo->tm_wday will be set */
	return mktime(&timeinfo);
}

void NMGTray::notify(bool baloon)
{
	time_t gameTime;
	DWORD size = sizeof(gameTime);


	if (RegGetValue(hKey, PARAMETERS, GAME_TIME, RRF_RT_REG_QWORD, nullptr, &gameTime, &size) == ERROR_SUCCESS) {

		time_t restTime = min((MAX_GAME_TIME - gameTime), getTime(LATE_HOURS, 0));

		time_t h = restTime / 3600;
		time_t m = (restTime - h * 3600) / 60;



		if ((restTime == REMIAND_BEFORE)
		|| (restTime == REMIAND_BEFORE / 2)
		|| (m == 0 && h != MAX_GAME_TIME/60/60))
		{
			baloon = true;
		}

		std::wostringstream out;

		out << "Rest game time - ";
		out << std::setfill(L'0');
		out << std::setw(2) << h << ":" << std::setw(2) << m;
		std::wstring  str = out.str();
		
		nid.uFlags = NIF_ICON | NIF_TIP | NIF_GUID | NIF_MESSAGE;
		StringCchCopy(nid.szTip, ARRAYSIZE(nid.szTip), str.c_str());

		if (baloon)
		{
			nid.uFlags |= NIF_INFO;
			StringCchCopy(nid.szInfo, ARRAYSIZE(nid.szInfo), str.c_str());
			StringCchCopy(nid.szInfoTitle, ARRAYSIZE(nid.szInfoTitle), BIG_BRO);
			nid.dwInfoFlags = NIIF_INFO;
		}

		Shell_NotifyIcon(NIM_MODIFY, &nid) ? true : false;

	}
}

void NMGTray::deinitNotify() {
	Shell_NotifyIcon(NIM_DELETE, &nid);
}

void NMGTray::popup() const {
	POINT pt;
	GetCursorPos(&pt);
	HMENU hMenu = CreatePopupMenu();

	AppendMenuW(hMenu, MF_STRING, IDM_ABOUT, L"A&bout");
	AppendMenuW(hMenu, MF_STRING, IDM_EXIT, L"E&xit");
	SetForegroundWindow(hWnd);
	TrackPopupMenu(hMenu, TPM_BOTTOMALIGN | TPM_LEFTALIGN, pt.x, pt.y, 0, hWnd, NULL);
}




#define MAX_LOADSTRING 100

// ���������� ����������:
HINSTANCE hInst;                                // ������� ���������
WCHAR szTitle[MAX_LOADSTRING];                  // ����� ������ ���������
WCHAR szWindowClass[MAX_LOADSTRING];            // ��� ������ �������� ����

// ��������� ���������� �������, ���������� � ���� ������ ����:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: ���������� ��� �����.

    // ������������� ���������� �����
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_NMGTRAY, szWindowClass, MAX_LOADSTRING);

	
    MyRegisterClass(hInstance);

    // ��������� ������������� ����������:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }
	
    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_NMGTRAY));

    MSG msg;

    // ���� ��������� ���������:
    while (true)
	{
		GetMessage(&msg, nullptr, 0, 0);
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {

            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}

//
//  �������: MyRegisterClass()
//
//  ����������: ������������ ����� ����.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_NMGTRAY));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_NMGTRAY);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   �������: InitInstance(HINSTANCE, int)
//
//   ����������: ��������� ��������� ���������� � ������� ������� ����.
//
//   �����������:
//
//        � ������ ������� ���������� ���������� ����������� � ���������� ����������, � �����
//        ��������� � ��������� �� ����� ������� ���� ���������.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // ��������� ���������� ���������� � ���������� ����������

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   //ShowWindow(hWnd, nCmdShow);
   //UpdateWindow(hWnd);

   return TRUE;
}

//
//  �������: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  ����������:  ������������ ��������� � ������� ����.
//
//  WM_COMMAND � ���������� ���� ����������
//  WM_PAINT � ���������� ������� ����
//  WM_DESTROY � ��������� ��������� � ������ � ���������
//
//
NMGTray *tray;

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
    {
	case WM_TIMER:
		tray->notify();
	case WM_CREATE:
		tray = new NMGTray(hWnd);
		break;
	case IDC_TRAY:
	{
		int wmId = LOWORD(lParam);
		switch (wmId)
		{
		case WM_LBUTTONUP:
			tray->notify(true);
			break;
		case WM_RBUTTONUP:
			tray->popup();
			break;
		};
		break;
	}
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // ��������� ����� � ����:
            switch (wmId)
            {
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: �������� ���� ����� ��� ����������, ������������ HDC...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
		delete tray;
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}



// ���������� ��������� ��� ���� "� ���������".
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
