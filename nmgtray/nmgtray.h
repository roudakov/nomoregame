#pragma once

#include "resource.h"
#include <Shellapi.h>

class NMGTray
{
public:
	NMGTray(HWND hWnd);
	~NMGTray();

	void notify(bool baloon = false);
	void popup() const;

private:
	HWND hWnd;
	HKEY hKey;
	NOTIFYICONDATA nid = {};

	void initRegistry();
	void deinitRegistry() const;

	void initNotify();
	void deinitNotify();
};